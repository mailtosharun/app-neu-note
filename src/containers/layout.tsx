import * as React from "react";
import { Container, Row, Col, Navbar, NavbarBrand } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStickyNote } from "@fortawesome/free-solid-svg-icons";
import { AddNew } from "../components/note/addnew";
import { NoteModal } from "../components/note/notemodal";
import { Notes } from "../components/note/notes";
import { ErrorBoundary } from "../components/error/errorboundary";

export const Layout = () => {
  return (
    <div>
      <ErrorBoundary>
        <Navbar color="light" light expand="md" className="navbar">
          <NavbarBrand href="/">
            <FontAwesomeIcon icon={faStickyNote} />
            &nbsp;My Notes
          </NavbarBrand>
        </Navbar>
        <Container fluid className="h-100 d-inline-block">
          <Row xs="4">
            <Col>
              <AddNew />
            </Col>
          </Row>
          <Notes />
          <NoteModal />
        </Container>
      </ErrorBoundary>
    </div>
  );
};
