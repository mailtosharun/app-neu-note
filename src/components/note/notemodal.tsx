import React, { useContext, useState, useEffect } from "react";
import { context } from "../../context/appcontext";
import { Editor } from "@tinymce/tinymce-react";
import { AppConstants } from "../../constants/appconstants";
import {
  Button,
  Modal,
  ModalBody,
  Form,
  Label,
  FormGroup,
  Input,
  Alert,
  ModalFooter
} from "reactstrap";
import { StringUtils } from "../../utils/stringUtils";
import { INote } from "../../models/note";
import { GithubPicker } from "react-color";
export const NoteModal = () => {
  const ctx = useContext(context);
  const [isInvalid, SetValidity] = useState<boolean>(false);

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    SetValidity(false);
    ctx.actions.editNote({ ...ctx.state.selectedNote, [name]: value });
  };

  useEffect(() => {
    const onbeforeunloadFn = () => {
      localStorage.setItem("data", JSON.stringify(ctx.state.selectedNote));
      processNote();
    };

    window.addEventListener("beforeunload", onbeforeunloadFn);
    return () => {
      window.removeEventListener("beforeunload", onbeforeunloadFn);
    };
  }, []);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    processNote();
  };

  const processNote = () => {
    if (validate()) {
      SetValidity(true);
    } else {
      ctx.state.selectedNote.id === 0 ? saveNote() : updateNote();
      SetValidity(false);
      ctx.actions.toggleModal();
    }
  };

  const handleEditorChange = (e: any) => {
    ctx.actions.editNote({
      ...ctx.state.selectedNote,
      description: e.target.getContent()
    });
    SetValidity(false);
  };

  const handleColorChange = (color: any) => {
    ctx.actions.editNote({ ...ctx.state.selectedNote, color: color.hex });
  };

  const saveNote = () => {
    let newNote: INote = { ...ctx.state.selectedNote, id: Date.now() };
    ctx.actions.saveNote(newNote);
  };

  const updateNote = () => {
    ctx.actions.updateNote(ctx.state.selectedNote);
  };

  const validate = () => {
    return (
      StringUtils.isNullOrEmpty(ctx.state.selectedNote.title) &&
      StringUtils.isNullOrEmpty(ctx.state.selectedNote.description)
    );
  };

  return (
    <div>
      <Modal isOpen={ctx.state.iSModalOpen} toggle={ctx.actions.toggleModal}>
        <ModalBody className="notemodal">
          <Form onSubmit={e => handleSubmit(e)}>
            <FormGroup>
              <Label for="Title">Title</Label>
              <Input
                autoComplete="off"
                type="text"
                name="title"
                id="title"
                value={ctx.state.selectedNote.title}
                onChange={e => handleInputChange(e)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="Description">Description</Label>

              <Editor
                apiKey={AppConstants.Editor.apiKey}
                initialValue={ctx.state.selectedNote.description}
                init={{
                  selector: "",
                  height: AppConstants.Editor.height,
                  menubar: AppConstants.Editor.menubar,
                  plugins: AppConstants.Editor.plugins,
                  toolbar: AppConstants.Editor.toolbar,
                  textpattern_patterns: AppConstants.Editor.textpattern_patterns
                }}
                onChange={e => handleEditorChange(e)}
              />
            </FormGroup>
            <GithubPicker
              onChangeComplete={handleColorChange}
              triangle="hide"
            />
            {isInvalid && (
              <Alert color="danger">Please provide any value</Alert>
            )}
            <ModalFooter>
              <Button color="primary" type="submit">
                Save
              </Button>{" "}
              <Button
                color="secondary"
                onClick={() => ctx.actions.toggleModal()}
              >
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
};

export interface noteModalProps {
  data: INote;
}
