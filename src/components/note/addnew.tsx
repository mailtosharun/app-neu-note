import React, { useContext } from "react";
import { context } from "../../context/appcontext";
import { IContext } from "../../models/context";
import { Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

export const AddNew = () => {
  const ctx: IContext = useContext(context);
  return (
    <div>
      <Button className="addnew" onClick={() => ctx.actions.toggleModal()}>
        <FontAwesomeIcon icon={faPlus} size="2x" />
      </Button>
    </div>
  );
};
