import React, { useContext, useEffect } from "react";
import { context } from "../../context/appcontext";
import { Col, Row } from "reactstrap";
import { Note } from "./note";
import { IContext } from "../../models/context";
import Draggable from "react-draggable";
export const Notes = () => {
  const ctx: IContext = useContext(context);
  const allnotes = ctx.state.notes.map((note, index) => {
    return (
      <Col>
        <Draggable>
          <div>
            <Note key={index} data={note} />
          </div>
        </Draggable>
      </Col>
    );
  });

  useEffect(() => {
    ctx.actions.getNotes();
  }, []);

  return (
    <div>
      {ctx.state.notes.length === 0 && (
        <Row>
          <div className="nonotestext">Notes are empty</div>
        </Row>
      )}
      <Row xs="1" sm="2" md="4">
        {allnotes}
      </Row>
    </div>
  );
};
