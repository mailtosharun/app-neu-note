import React, { Fragment, useContext } from "react";
import { INote } from "../../models/note";
import { Card, CardBody, CardText, CardTitle } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import ReactTooltip from "react-tooltip";
import { context } from "../../context/appcontext";
import { IContext } from "../../models/context";
export const Note = (props: INoteProps) => {
  const ctx: IContext = useContext(context);
  var style = { backgroundColor: props.data.color };
  return (
    <Fragment>
      <Card body style={style} className="note">
        <div>
          <span className="notedate">
            {new Date(props.data.id).toLocaleDateString()}{" "}
          </span>
          <span className="noteaction">
            <a
              onClick={() => {
                ctx.actions.editNote(props.data);
              }}
            >
              <FontAwesomeIcon icon={faEdit} data-tip="edit" />
            </a>{" "}
            &nbsp;
            <a
              onClick={() => {
                ctx.actions.deleteNote(props.data.id);
              }}
            >
              <FontAwesomeIcon icon={faTrash} data-tip="delete" />
            </a>{" "}
          </span>
        </div>
        <CardBody>
          <CardTitle className="notetitle">
            <b>{props.data.title}</b>
          </CardTitle>
          <hr />
          <CardText>
            <div
              dangerouslySetInnerHTML={{ __html: props.data.description }}
            ></div>
          </CardText>
        </CardBody>
      </Card>
      <ReactTooltip />
    </Fragment>
  );
};

export interface INoteProps {
  data: INote;
}
