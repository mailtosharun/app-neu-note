import React, { Component } from "react";
import { LocalStorageService } from "../../services/localstorage.service";
export class ErrorBoundary extends Component {
  public state = { hasError: false };
  constructor(props: any) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error: any) {
    return { hasError: true };
  }

  componentDidCatch(error: any, errorInfo: any) {
    LocalStorageService.saveData(
      "errorLog",
      JSON.stringify({ error: error, info: errorInfo })
    );
  }

  render() {
    if (this.state.hasError) {
      return <h1>Oops! Something went wrong!</h1>;
    }

    return this.props.children;
  }
}
