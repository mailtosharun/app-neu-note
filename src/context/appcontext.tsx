import React, { Component, createContext } from "react";
import { IState } from "../models/state";
import { INote } from "../models/note";
import { NoteAction } from "../actions/noteaction";
import { IContext } from "../models/context";
export const context = createContext<any>({} as IContext);
const initial_state = {
  id: 0,
  title: "",
  color: "",
  description: "",
  order: ""
};
class AppProvider extends Component<{}, IState> {
  noteAction = new NoteAction();
  state: IState = {
    notes: [],
    selectedNote: initial_state,
    iSModalOpen: false
  };
  getNotes = () => {
    this.setState({ notes: this.noteAction.getNotes() });
  };
  saveNote = (note: INote) => {
    let newNotes = this.state.notes.slice();
    newNotes.unshift(note);
    this.setState(
      {
        notes: newNotes,
        selectedNote: initial_state
      },
      () => this.noteAction.saveNotes(newNotes)
    );
  };

  editNote = (note: INote) => {
    this.setState({ selectedNote: note, iSModalOpen: true });
  };

  updateNote = (note: INote) => {
    this.noteAction.updateNote(note);
    let updatedNotes = this.noteAction.getNotes();
    this.setState({
      notes: updatedNotes,
      selectedNote: initial_state
    });
  };
  deleteNote = (id: number) => {
    let updatedNotes = this.noteAction.deleteNote(id);
    this.setState({
      notes: updatedNotes,
      selectedNote: initial_state
    });
  };

  toggleModal = () => {
    this.setState({
      iSModalOpen: !this.state.iSModalOpen,
      selectedNote: initial_state
    });
  };

  render() {
    return (
      <context.Provider
        value={{
          state: this.state,
          actions: {
            getNotes: this.getNotes,
            toggleModal: this.toggleModal,
            saveNote: this.saveNote,
            updateNote: this.updateNote,
            deleteNote: this.deleteNote,
            editNote: this.editNote
          }
        }}
      >
        {this.props.children}
      </context.Provider>
    );
  }
}
export { AppProvider };
export const AppConsumer = context.Consumer;
