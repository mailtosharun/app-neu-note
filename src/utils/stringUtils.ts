export class StringUtils {
  static isNullOrEmpty(val: any): boolean {
    return val === undefined || val === null || val === "" || val.length === 0;
  }
}
