import { IState } from "./state";
import { IAction } from "./action";

export interface IContext {
  state: IState;
  actions: IAction;
}
