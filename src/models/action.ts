import { INote } from "./note";

export interface IAction {
  getNote: (id: string) => INote;
  getNotes: () => INote[];
  saveNotes: (notes: INote[]) => void;
  updateNote: (note: INote) => void;
  deleteNote: (id: number) => Array<INote>;
  toggleModal: () => void;
  editNote(note: INote): void;
}
