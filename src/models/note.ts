export interface INote {
  id: number;
  title: string;
  description: string;
  order: string;
  color: string;
}
