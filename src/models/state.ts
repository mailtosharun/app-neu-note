import { INote } from "./note";

export interface IState {
  notes: INote[];
  selectedNote: INote;
  iSModalOpen: boolean;
}
