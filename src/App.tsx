import * as React from "react";
import "./App.css";
import { Layout } from "./containers/layout";
import { AppProvider } from "./context/appcontext";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";

export const App = () => {
  return (
    <div className="App">
      <AppProvider>
        <Layout />
      </AppProvider>
      <ToastContainer position={toast.POSITION.TOP_RIGHT}></ToastContainer>
    </div>
  );
};
