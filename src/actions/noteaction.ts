import { IAction } from "../models/action";
import { INote } from "../models/note";
import { LocalStorageService } from "../services/localstorage.service";
import { toast } from "react-toastify";

export class NoteAction implements IAction {
  getNote(id: string): INote {
    let note: INote;
    note = JSON.parse(LocalStorageService.getData("notes") || "{}");
    return note;
  }

  getNotes(): Array<INote> {
    let notes: Array<INote> = [];
    notes = JSON.parse(LocalStorageService.getData("notes") || "[]");
    return notes;
  }

  saveNotes(notes: INote[]): void {
    LocalStorageService.saveData("notes", JSON.stringify(notes));
    toast.success("Note Saved");
  }

  updateNote(note: INote): void {
    let allNotes = this.getNotes();
    let notes = allNotes.slice();
    let index = notes.findIndex(x => x.id === note.id);
    notes.splice(index, 1, note);
    this.saveNotes(notes);
  }

  deleteNote(id: number): Array<INote> {
    let notes = this.getNotes();
    var updatedNotes = notes.filter(note => {
      return note.id !== id;
    });
    LocalStorageService.saveData("notes", JSON.stringify(updatedNotes));
    toast.success("Note Deleted");
    return updatedNotes;
  }

  toggleModal(): void {}
  editNote(note: INote): void {}
}
