export const AppConstants = {
  Editor: {
    toolbar: [
      "undo redo | bold italic | \
     alignleft aligncenter alignright | \
     bullist numlist outdent indent"
    ],
    plugins: [
      "advlist autolink lists link image",
      "charmap print preview anchor help",
      "searchreplace visualblocks code",
      "insertdatetime media table paste wordcount",
      "textpattern"
    ],
    textpattern_patterns: [
      { start: "*", end: "*", format: "italic" },
      { start: "**", end: "**", format: "bold" },
      { start: "#", format: "h1" },
      { start: "##", format: "h2" },
      { start: "###", format: "h3" },
      { start: "####", format: "h4" },
      { start: "#####", format: "h5" },
      { start: "######", format: "h6" },
      { start: "1. ", cmd: "InsertOrderedList" },
      { start: "* ", cmd: "InsertUnorderedList" },
      { start: "- ", cmd: "InsertUnorderedList" }
    ],
    apiKey: "cfor1eydfx5g9vbc046awj1gt81vogweh8np13g6o4tyw7yz",
    height: 300,
    menubar: true
  }
};
