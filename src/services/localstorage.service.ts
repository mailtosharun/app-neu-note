export class LocalStorageService {
  static getData(name: string) {
    return localStorage.getItem(name);
  }

  static saveData(name: string, data: any) {
    localStorage.setItem(name, data);
  }
}
